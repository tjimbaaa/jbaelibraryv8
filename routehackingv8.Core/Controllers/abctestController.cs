﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace SampleDemo.Core.Controllers
{
    public class abctestController : RenderMvcController
    {
        public ActionResult Index(ContentModel model)
        {
            Logger.Info<abctestController>($"{model.Content.Id} value of sugar is {Request.Params["sugar"]}");
            return CurrentTemplate(model);

        }

    }
}
